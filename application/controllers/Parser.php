<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: lanka
 * Date: 01.12.16
 * Time: 17:35
 */
class Parser extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database('default');
    }

    public function index()
    {
        $this->load->model('parser_model');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://www.invitro.ru/analizes/for-doctors/");
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_ENCODING , "UTF-8");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $output = curl_exec($ch);
        curl_close($ch);
//          конвертирует в utf|костыль
        $html = mb_convert_encoding($output, "utf-8", "windows-1251");

//        забирает div c таблицей (криво)
        preg_match('~<div\sid="catalog-section-analiz".+>([\S\s]+)(?=<\/div>)~', $html, $answer_check);
        preg_match_all('~<tr>([\S\s]*)<\/tr>~U', $answer_check[0], $tr);
        $trtr = $tr[0];
//          массив пришедших анализов
        foreach ($trtr as $key => $value)
        {
            if(preg_match_all('~<td class="number elem_list_[0,1]">([\S\s]*)<\/td>~U', $value, $code) and
                preg_match_all('~<td class="name elem_list_[0,1]"[\S\s]+>([\S\s]+)(?=<\/a>)~U', $value, $title) and
                preg_match_all('~<td class="price elem_list_[0,1]"[\S\s]+>(?<=</span>)([\S\s]+)(?=<\/td>)~U', $value, $price)) {
                $come_service[] = [
                    'title' => $title[1][0],
                    'code' => $code[1][0],
                    'price' => $price[1][0]
                ];
            }
        }
//        var_dump($come_service);
        $service_db['result'] = $this->parser_model->get_data();

        //          добавление и обновление записей
        foreach ($come_service as $test)
        {
//                var_dump($test);
            if($test['code']=='')
            {
                $answer_check = $this->in_array_r($test['title'],$service_db['result']);
//                var_dump($answer_check);
                if($answer_check['title']==$test['title'])
                {
                    if ($test['price'] != $answer_check['price'])
                    {
                        $set = ['price' => $test['price']];
                        $where = ['title' => $test['title']];
                        $this->parser_model->update_data($set, $where);
                    }
                }
            }
            else
            {
                $answer_check = $this->in_array_r($test['code'],$service_db['result']);
                if($answer_check['code']==$test['code'])
                {
                    if ($test['title'] != $answer_check['title'])
                    {
                        $set = ['title'=> $test['title']];
                        $where = ['code' => $test['code']];
                        $this->parser_model->update_data($set, $where);

                    }
                    if ($test['price'] != $answer_check['price'])
                    {
                        $set = ['price'=> $test['price']];
                        $where = ['code' => $test['code']];
                        $this->parser_model->update_data($set, $where);
                    }
                }
            }
            if( ($test['code']!='' and !($this->in_array_r($test['code'],$service_db['result'])))
                or ($test['code']=='' and !($this->in_array_r($test['title'],$service_db['result']))))
            {
                if (empty($test['code']))
                    $test['code'] = NULL;
                $set = [
                    'code' => $test['code'],
                    'title' => $test['title'],
                    'price' => $test['price']
                ];
                $this->parser_model->insert_data($set);
            }
        }
        //устанавливает признак is_deleted = true
        foreach ($service_db['result'] as $k)
        {
//            var_dump($k);
            if($k['code']!=NULL && !($this->in_array_r($k['code'],$come_service)))
            {
                $where = ['code' => $k['code']];
                $this->parser_model->delete_data($where);
            }
            else if($k['code']==NULL && !($this->in_array_r($k['code'],$come_service)))
            {
                $where = ['title' => $k['title']];
                $this->parser_model->delete_data($where);
            }
        }

        $this->load->view('parser_view', $service_db);
    }

    function in_array_r($needle, $haystack) {
//        var_dump($needle);
        foreach ($haystack as $item) {
            if(in_array($needle,$item))
            {
//                var_dump($item);
                return $item;
            }
        }
        return FALSE;
    }
}