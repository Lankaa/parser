<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: lanka
 * Date: 02.12.16
 * Time: 16:09
 */
class Parser_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

//        беру первый код из пришедшего массива ищу такой код в базе,
//        если есть, проверяю не изменилось ли название и цена, если нет, то ничего не меняю, если да,
//        тогда обновляю данные, и тд, если не нахожу нужный код тогда добавляю новую запись в бд,
//        если код == null, тогда делаю тоже самое только по названию
//        удаление делаю наоборот -  сравниваю коды,
//        и если есть в бд, но нет на странице, удаляю элемент из бд

    public function get_data()
    {
        $this->db->select('title, code, price, is_deleted');
        $query = $this->db->get('service');
        return $query->result_array();
    }

    public function update_data($change, $place)
    {

        $this->db->set($change)->update('service')->where($place);
//        echo $this->db->set($change)->get_compiled_update('service')->where($place);
    }

    public function insert_data($add)
    {
        $this->db->set($add)->insert('service');
//        echo $this->db->set($add)->get_compiled_insert('service');
    }

    public function delete_data($place)
    {
        $this->db->set(['is_deleted' => true])->update('service')->where($place);
//        echo $this->db->set(['is_deleted' => true])->get_compliled_update('service')->where($place);
    }

}